# _Garden 7_

    $ This is the Garden/ Project of Jonas Pfalzgraf for the graduation at the
    $ BMK Hamburg in the Screen Development Class.

#### _The full Project is developt under MIT license_


         ____.               __   _________ __                _______                              .__
        |    |__ __  _______/  |_/   _____//  |______  ___.__.\      \   ___________  _____ _____  |  |
        |    |  |  \/  ___/\   __\_____  \\   __\__  \<   |  |/   |   \ /  _ \_  __ \/     \\__  \ |  |
    /\__|    |  |  /\___ \  |  | /        \|  |  / __ \\___  /    |    (  <_> )  | \/  Y Y  \/ __ \|  |__
    \________|____//____  > |__|/_______  /|__| (____  / ____\____|__  /\____/|__|  |__|_|  (____  /____/
                        \/              \/           \/\/            \/                   \/     \/
